var webpack = require('webpack');

module.exports = {
    entry: './src/ts/main.ts',
    output: {
        filename: 'bundle.js',
    },
    resolve: {
        extensions: ['.webpack.js', '.web.js', '.ts', '.js', '.styl'],
        alias: {
            "jquery": `${__dirname}/jquery-stub.js`,
        },
    },
    module: {
        loaders: [
            { test: /\.ts$/, loader: 'ts-loader' },
            { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' },
        ],
    },
}
