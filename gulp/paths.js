export default {
    app: './app',
    vendor: './node_modules',
    src: {
        ts: './src/ts',
        js: './src/js',
        jade: './src/jade',
        twig: './src/twig',
        js: './src/js',
        stylus: './src/stylus',
        html: './src/html',
        icons: './src/icons',
        iconsSVG: './src/icons/svg',
        images: './src/images',
    },
    dist: {
        js: '../app/Resources/public/js',
        css: '../app/Resources/public/css',
        images: '../app/Resources/public/images'
    }
}
