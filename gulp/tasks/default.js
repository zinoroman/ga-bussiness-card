import gulp from 'gulp';

gulp.task('default', [
        'stylus:default',
        'scripts:default',
        `sprite-svg:default`,
        `images:default`,
]);

gulp.task('build', [
    'stylus:build',
    'scripts:build',
    'sprite-svg:create',
    'images:optimize'
]);
