import { BusinessCardCore } from './../core/core';
import { BusinessCardCanvases } from '../canvases/canvases';
import 'bootstrap-colorpicker';
import { EVENT_CANVAS_SIDE_CHANGED } from '../canvases/canvases.events';

export class BusinessCardCanvasesBackground extends BusinessCardCore {

    private element: JQuery = $('[data-action="business-card.canvas-background-color"]');

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);
        this.listen();
    }

    private listen() {
        this.listenEvents();
        this.listenDOM();
    }

    private listenEvents() {
        this.businessCardCanvases.events.addListener(EVENT_CANVAS_SIDE_CHANGED, () => {
            const canvasBackgroundColor = this.businessCardCanvases.canvas['get']('backgroundColor');

            if (canvasBackgroundColor) {
                this.element.colorpicker('setValue', canvasBackgroundColor);
            } else {
                this.element.css('background-color', '');
                this.element.colorpicker('setValue', this.element.css('background-color'));
            }
        });
    }

    private listenDOM() {
        this.element
        .colorpicker()
        .on('changeColor', (event) => {
            const selectedColor = event.color.toString('rgba');

            this.setToggleElementBackgoundColor(selectedColor)
            this.onCanvasesBackgroundColorSelected(selectedColor);
        });
    }

    private setToggleElementBackgoundColor(backgroundColor: string) {
        this.element.css('background-color', backgroundColor);
    }

    private onCanvasesBackgroundColorSelected(backgroundColor: string) {
        this.businessCardCanvases.canvas.setBackgroundColor(backgroundColor, () => {
            this.businessCardCanvases.renderAll();
        });
    }
}