import { BusinessCardControlPanelItem } from './../control-panel-item/control-panel-item';
import { BusinessCardCanvases } from '../canvases/canvases';
import * as f from 'fabric'; const fabric: typeof f = (f as any).fabric;
import 'bootstrap-slider';

export class BusinessCardObjectOpacity extends BusinessCardControlPanelItem {

    public selector: string = '[data-action="business-card.object-opacity"]';
    private element: JQuery = $(this.selector);
    private supportedObjectTypes: string[] = [
        'text',
        'i-text',
        'image',
    ];

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);
        this.listen();
    }

    private listen() {
        if (this.canEventListenerBeAttached(this.element[0])) {
            this.element.each((index: number, element: Element) => {
                $(element)
                    .slider({
                        tooltip: 'hide',
                    }).on('slide', (event: Event) => this.onOpacityChanged(event));
            });

            this.businessCardCanvases.forEach((canvas: fabric.Canvas) => {
                canvas.on('object:selected', (event: { e: Event, target: fabric.Object }) => this.onObjectSelected(event.target));
                canvas.on('selection:cleared', (event: { e: Event, target: fabric.Object }) => this.onObjectUnselected());
            });
        }
    }

    private onObjectSelected(object: fabric.Object) {
        if (this.canBeOpacityApplied(this.supportedObjectTypes, object.type)) {
            this.element.each((index: number, element: HTMLInputElement) => {
                // On the page can be several inputs that controls opacity
                // So we have to find one that is responsible for current object data type
                const $element = $(element);
                if ($element.data('object-types').split('|').indexOf(object.type) > -1) {
                    $element.slider('setValue', object.getOpacity() * 100);
                }
            });
        } else {
            this.setDefaultOpacityValueOnElement();
        }
    }

    private onObjectUnselected() {
        this.setDefaultOpacityValueOnElement();
    }

    private onOpacityChanged(event: Event) {
        const activeObject: fabric.Object = this.businessCardCanvases.getActiveObject();

        if (activeObject) {
            const target: HTMLInputElement = <HTMLInputElement> event.target;
            const value: number = parseInt(target.value, 10);
            const supportedObjectTypes: string[] = target.dataset.objectTypes.split('|');
            
            if (this.canBeOpacityApplied(supportedObjectTypes, activeObject.type)) {
                activeObject.setOpacity(value / 100);
                this.businessCardCanvases.renderAll();
            }
        }
    }

    private canBeOpacityApplied(supportedObjectTypes: string[], objectType: string): boolean {
        return supportedObjectTypes.indexOf(objectType) > -1;
    }

    private setDefaultOpacityValueOnElement() {
        this.element.slider('setValue', 100);
    }

}
