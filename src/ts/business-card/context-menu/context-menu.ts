import { BusinessCardCanvases } from './../canvases/canvases';
import { BusinessCardCore } from '../core/core';
import './context-menu.styl';

export class BusinessCardContextMenu extends BusinessCardCore {
    
    private selector: string = '#business-card-canvas-context-menu';
    private element: HTMLUListElement|null = <HTMLUListElement|null> document.querySelector(this.selector);

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);

        this.initCustomContextMenu();
        this.listen();
    }

    private initCustomContextMenu() {
        Array.prototype.forEach.call(this.businessCardCanvases.canvasesContainer.getElementsByTagName('canvas'), (element: HTMLCanvasElement) => {
            element.addEventListener('contextmenu', (event) => {
                if (this.businessCardCanvases.canvas.getActiveObject()) {
                    Object.assign(this.element.style, {
                        left: `${event.pageX}px`,
                        top: `${event.pageY}px`,
                    });

                    this.showContextMenu();
                }

                event.preventDefault();
            });
        });

        document.addEventListener('click', (event) => {
            const target: HTMLElement = <HTMLElement> event.target;
            this.hideContextMenu();
        })
    }

    private listen() {
        this.handleDelete();
        this.handleBringForward();
        this.handleBringToFront();
        this.handleSendBackwards();
        this.handleSendToBack();
    }

    private handleDelete() {
        document.getElementById('business-card-canvas-context-menu-delete').addEventListener('click', (event: Event) => {
            this.businessCardCanvases.removeObject(this.businessCardCanvases.getActiveObject());
            this.hideContextMenu();
        });
    }

    private handleBringForward() {
        document.getElementById('business-card-canvas-context-bring-forward').addEventListener('click', (event: Event) => {
            this.businessCardCanvases.getActiveObject().bringForward();
            this.hideContextMenu();
        });
    }

    private handleBringToFront() {
        document.getElementById('business-card-canvas-context-bring-to-front').addEventListener('click', (event: Event) => {
            this.businessCardCanvases.getActiveObject().bringToFront();
            this.hideContextMenu();
        });
    }

    private handleSendBackwards() {
        document.getElementById('business-card-canvas-context-send-backward').addEventListener('click', (event: Event) => {
            this.businessCardCanvases.getActiveObject().sendBackwards();
            this.hideContextMenu();
        });
    }

    private handleSendToBack() {
        document.getElementById('business-card-canvas-context-send-to-back').addEventListener('click', (event: Event) => {
            this.businessCardCanvases.getActiveObject().sendToBack();
            this.hideContextMenu();
        });
    }

    private showContextMenu() {
        this.element.classList.remove('hidden');
    }

    private hideContextMenu() {
        this.element.classList.add('hidden');
    }
}