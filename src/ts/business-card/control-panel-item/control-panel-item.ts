import { BusinessCardCore } from './../core/core';

export abstract class BusinessCardControlPanelItem extends BusinessCardCore {

    abstract selector: string;

    protected canEventListenerBeAttached(toggle: HTMLElement|null) {
        return !!toggle;
    }

    protected toggleIsAbsentError() {
        throw new Error(`${this.selector} element is required for proper work`);
    }
}