import { BusinessCardCanvases } from './../canvases/canvases';

export class BusinessCardCore {

    constructor(public businessCardCanvases: BusinessCardCanvases) {
    }
}
