declare const wedes: any;

import { BusinessCardCanvases } from './../canvases/canvases';
import { BusinessCardCore } from '../core/core';
import * as f from 'fabric'; const fabric: typeof f = (f as any).fabric;
import 'jquery-form';
import swal from 'sweetalert2';

export class BusinessCardImage extends BusinessCardCore {
    
    private imageForm = $('[data-action="business-card.image-loader"]');
    private inputFile: JQuery = $('#canvas-business-card-file');

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);
        this.listen();
    }

    private listen() {
        this.inputFile.on('change', () => this.uploadImageFile());
    }

    private onUserSelectImage(imageFile: File) {
        this.convertImageFileToURL(imageFile);
    }

    private uploadImageFile() {
        const inputFileWrapperButton = this.inputFile.closest('.btn');
        const classNameLoading = 'btn-loading';

        inputFileWrapperButton.addClass(classNameLoading);

        this.imageForm
            .ajaxSubmit()
            .data('jqxhr')
            .done((response: { sources: { image_preview: string, original: string }, token_value: string }) => {
                this.addImageToCanvas(response.sources.original);
            }).fail(() => {
                swal({
                    title: wedes.translations.imageLoadingAlertUploadingError.title, 
                    text: wedes.translations.imageLoadingAlertUploadingError.message,
                    confirmButtonColor: '#81081b',
                });
            }).always(() => {
                inputFileWrapperButton.removeClass(classNameLoading);
            });
    }

    private convertImageFileToURL(file: File) {
        const fileReader = new FileReader();

        fileReader.addEventListener('load', this.onFileReaderLoaded.bind(this));
        fileReader.readAsDataURL(file);
    }

    private onFileReaderLoaded(fileLoadedEvent) {
        const base64Image: string = fileLoadedEvent.target.result;
        this.addImageToCanvas(base64Image);
    }

    private addImageToCanvas(imageUrl: string) {
        fabric.Image.fromURL(imageUrl, (image: fabric.Image) => {
            const imageGap = 70;
            const desirableImageDimensions: { width: number, height: number } = {
                width: this.businessCardCanvases.canvas.getWidth() - imageGap,
                height: this.businessCardCanvases.canvas.getHeight() - imageGap,
            };
            const imageDimensions: { width: number, height: number } = {
                width: image.width,
                height: image.height,
            };

            if (imageDimensions.height > desirableImageDimensions.height) {
                image.scaleToHeight(desirableImageDimensions.height);
            } else if (imageDimensions.width > desirableImageDimensions.width) {
                image.scaleToWidth(desirableImageDimensions.width);
            }

            this.businessCardCanvases.canvas.add(image);

            image.lockUniScaling = true;
            image.lockScalingFlip = true;
            image.center();
            image.setCoords();

            this.resetInput();
        });
    }

    private resetInput() {
        this.inputFile.value = null;
    }
}
