declare const wedes;

import { BusinessCardCore } from '../core/core';
import { BusinessCardCanvases } from './../canvases/canvases';
import { BusinessCardSize } from './../size/size';
import swal from 'sweetalert2';

export class BusinessCardPreview extends BusinessCardCore {

    constructor(public businessCardCanvases: BusinessCardCanvases,
                public businessCardSize: BusinessCardSize) {
        super(businessCardCanvases);
        this.listen();
    }

    private listen() {
        Array.prototype.forEach.call(
            document.querySelectorAll('[data-action="business-card.preview"]'), 
            (element: HTMLElement) => {
                element.addEventListener('click', (event: Event) => this.onPreviewRequested(event));
            },
        );
    }

    private onPreviewRequested(event: Event) {
        this.showPreview();
        event.preventDefault();
    }

    private showPreview() {
        const mmToPxCoefficient = 3.779528;
        const canvasSizeInMM = this.businessCardSize.getActiveSizeInMM();

        swal({
            imageUrl: this.businessCardCanvases.canvas.toDataURL({
                format: 'png',
            }),
            imageWidth: canvasSizeInMM.width * mmToPxCoefficient,
            imageHeight: canvasSizeInMM.height * mmToPxCoefficient,
            confirmButtonColor: '#81081b',
        });
    }
}
