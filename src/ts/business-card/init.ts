import { BusinessCardCanvasesBackground } from './canvases-background/canvases-background';
import { BusinessCardKeyboard } from './keyboard/keyboard';
import { BusinessCardContextMenu } from './context-menu/context-menu';
import { BusinessCardObjectOpacity } from './object-opacity/object-opacity';
import { BusinessCardCanvases } from './canvases/canvases';
import { BusinessCardOverlayImage } from './overlay-image/overlay-image';
import { BusinessCardImage } from './image/image';
import { BusinessCardText } from './text/text';
import { BusinessCardSize } from './size/size';
import { BusinessCardRounded } from './rounded/rounded';
import { BusinessCardImageEffects } from './image-effects/image-effects';
import { BusinessCardPreview } from './preview/preview';
import { BusinessCardOrder } from './order/order';

export function initBusinessCard() {
    const businessCardCanvases = new BusinessCardCanvases();
    const businessCardImage = new BusinessCardImage(businessCardCanvases);
    const businessCardText = new BusinessCardText(businessCardCanvases);
    const businessCardSize = new BusinessCardSize(businessCardCanvases);
    const businessCardImageEffects = new BusinessCardImageEffects(businessCardCanvases);
    const businessCardObjectOpacity = new BusinessCardObjectOpacity(businessCardCanvases);
    const businessCardContextMenu = new BusinessCardContextMenu(businessCardCanvases);
    const businessCardKeyboard = new BusinessCardKeyboard(businessCardCanvases);
    const businessCardCanvasesBackground = new BusinessCardCanvasesBackground(businessCardCanvases);
    const businessCardRounded = new BusinessCardRounded(businessCardCanvases);
    const businessCardPreview = new BusinessCardPreview(businessCardCanvases, businessCardSize);
    const businessCardOverlayImage = new BusinessCardOverlayImage(businessCardCanvases, businessCardSize, businessCardRounded);
    const businessCardOrder = new BusinessCardOrder(businessCardCanvases);

    return {
        businessCardCanvases,
    };
}
