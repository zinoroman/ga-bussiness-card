export class BusinessCardStylishFontWeightObjectStrategy {
    public execute(options: { currentValue: string, activeValue: string, DOMToggle: JQuery, fabricObject: fabric.Object }) {
        if (!options.currentValue || options.currentValue !== options.activeValue) {
            options.fabricObject.set('fontWeight', options.activeValue);
            options.DOMToggle.addClass('active');
        } else {
            options.fabricObject.set('fontWeight', options.fabricObject.data.fontWeightDefault);
            options.DOMToggle.removeClass('active');
        }
    }
}