import { BusinessCardCore } from '../core/core';
import { BusinessCardCanvases } from '../canvases/canvases';
import * as f from 'fabric'; const fabric: typeof f = (f as any).fabric;
import swal from 'sweetalert2';
import * as Mustache from 'mustache';
import 'bootstrap-colorpicker';
import { EVENT_CANVAS_OBJECT_REMOVED, EVENT_CANVAS_SIDE_CHANGED, EVENT_CANVAS_OBJECT_LOADED_FROM_STRING, EVENT_CANVAS_REQUESTED_LOADING_FROM_STRING } from '../canvases/canvases.events';

export class BusinessCardText extends BusinessCardCore {

    private templates: { [propName: string]: string } = {
        textField: document.getElementById('canvas-business-card-text-field-template').innerHTML,
    };

    private textFieldsContainer: { front: HTMLElement, back: HTMLElement } = {
        front: <HTMLElement> document.getElementById('canvas-business-card-text-fields-front'),
        back:  <HTMLElement> document.getElementById('canvas-business-card-text-fields-back'),
    };

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);
        this.listen();
    }

    private listen() {
        this.listenStyleText();
        this.listenChangeFontWeight();
        this.listenChangeTextFontFamily();
        this.listenChangeTextFontSize();
        this.listenTextAdd();
        this.listenChangeTextColor();
        this.listenChangeTextBackgroundColor();
        this.listenCanvasChange();
        this.listenTextEditingOnCanvas();
        this.listenTextRemoving();
        this.listenCanvasSideChange();
        this.listenRequestedLoadingFromString();
        this.listenCanvasObjectLoadedFromString();
    }

    private listenCanvasSideChange() {
        this.businessCardCanvases.events.addListener(EVENT_CANVAS_SIDE_CHANGED, (side: string) => {
            Object.keys(this.textFieldsContainer).forEach((objectKey: string) => {
                const targetClassList = this.textFieldsContainer[objectKey].classList;

                if (objectKey !== side) {
                    targetClassList.add('hidden');
                } else {
                    targetClassList.remove('hidden');
                }
            });
        });
    }

    private listenCanvasChange() {
        this.businessCardCanvases.forEach((canvas: fabric.Canvas) => {
            canvas.on(
                'object:selected',
                (event: { e: Event, target: fabric.Object }) => this.onObjectSelected(event.target),
            );
            canvas.on(
                'selection:cleared',
                (event: { e: Event, target: fabric.Object }) => this.onObjectUnselected(),
            );
        });
    }

    private onObjectSelected(object: fabric.Object) {
        if (this.isObjectText(object)) {
            const textId = object.data.id;
            const textFill = (object as fabric.IText).getFill();
            const textBackgroundColor = (object as fabric.IText).getTextBackgroundColor();
            const fontSize = (object as fabric.IText).getFontSize();
            const fontFamily = (object as fabric.IText).getFontFamily();
            const fontWeight = (object as fabric.IText).getFontWeight();
            const fontStyle = (object as fabric.IText).getFontStyle();
            const textDecoration = (object as fabric.IText).getTextDecoration();
            const textAlign = (object as fabric.IText).getTextAlign();

            if (textFill) {
                $('[data-action="business-card.text-color"]').colorpicker('setValue', textFill);
            }

            if (textBackgroundColor) {
                $('[data-action="business-card.text-background-color"]').colorpicker('setValue', textBackgroundColor);
            }

            if (fontSize) {
                $('select[data-action="change-text-font-size"]').val(fontSize).selectpicker('refresh');
            }

            if (fontFamily) {
                $('select[data-action="change-text-font-family"]').val(fontFamily).selectpicker('refresh');
            }

            if (fontWeight === 700) {
                $('[data-action="style-font-weight"]').addClass('active');
            } else {
                $('[data-action="style-font-weight"]').removeClass('active');
            }

            if (fontStyle === 'italic') {
                $('[data-action="style-text"][data-type="fontStyle"]').addClass('active');
            }

            if (textDecoration) {
                $(`[data-action="style-text"][data-type="textDecoration"][data-value-active="${textDecoration}"]`).addClass('active');
            }

            if (textAlign) {
                $(`[data-action="style-text"][data-type="textAlign"][data-value-active="${textAlign}"]`).addClass('active');
            }
        }
    }

    private onObjectUnselected() {
        $('[data-action="business-card.text-color"]').css('background-color', '');
        $('[data-action="business-card.text-background-color"]').css('background-color', '');
        // $('select[data-action="change-text-font-size"]').val(null).selectpicker('refresh');
        // $('select[data-action="change-text-font-family"]').val(null).selectpicker('refresh');
        $('[data-action="style-font-weight"]').removeClass('active');
        $('[data-action="style-text"].active').removeClass('active');
    }

    private listenStyleText() {
        const styleTextToggles = $('button[data-action="style-text"]');

        styleTextToggles.click((event) => {
            this.stylishActiveTextObjects((activeObject: fabric.Object) => {
                const button: JQuery = $(event.currentTarget);

                const type = button.data('type');
                const values: { active: string, default: string } = {
                    active: button.data('value-active'),
                    default: button.data('value-default'),
                };

                if (!activeObject[type] || activeObject[type] !== values.active) {
                    activeObject.set(type, values.active);
                    button.addClass('active');
                    styleTextToggles
                        .filter(`[data-type="${type}"]`)
                            .not(button)
                            .removeClass('active');
                } else {
                    activeObject.set(type, values.default);
                    button.removeClass('active');
                }
            });

            event.preventDefault();
        });
    }

    private listenChangeFontWeight() {
        const toggles = $('button[data-action="style-font-weight"]');

        toggles.click((event) => {
            this.stylishActiveTextObjects((activeObject: fabric.Object) => {
                const button: JQuery = $(event.currentTarget);
                const type = 'fontWeight';
                const activeValue = 700;

                console.log(activeObject.data);

                if (!activeObject[type] || activeObject[type] !== activeValue) {
                    activeObject.set(type, activeValue);
                    button.addClass('active');
                } else {
                    activeObject.set(type, activeObject.data.fontWeightDefault);
                    button.removeClass('active');
                }
            });

            event.preventDefault();
        });
    }

    private listenChangeTextFontFamily() {
        $('select[data-action="change-text-font-family"]')
            .selectpicker()
            .change((event) => {
                this.stylishActiveTextObjects((activeObject: fabric.IText) => {
                    activeObject.setFontFamily((event.target as HTMLSelectElement).value);
                });

                event.preventDefault();
            });
    }

    private listenChangeTextFontSize() {
        Array.prototype.forEach.call(document.querySelectorAll('select[data-action="change-text-font-size"]'), (element: HTMLButtonElement) => {
            element.addEventListener('change', (event) => {
                this.stylishActiveTextObjects((activeObject: fabric.Object) => {
                    activeObject.set('fontSize', (event.target as HTMLSelectElement).value);
                });

                event.preventDefault();
            });
        });
    }

    private listenTextAdd() {
        document.querySelector('[data-action="business-card.add-field-add-text"]').addEventListener('click', (event) => {
            const fabricTextObjectId: string = this.generatetextFieldId();
            
            this.addTextToCanvas(
                this.addTextToDOM(
                    this.textFieldsContainer[this.businessCardCanvases.getActiveCanvasSide()],
                    fabricTextObjectId,
                ).value, 
                fabricTextObjectId,
            );

            event.preventDefault();
        });
    }
    
    private addTextToDOM(textFieldContainer: HTMLElement, fabricTextObjectId: string): HTMLTextAreaElement {
        const textFieldTextareaWrapper: HTMLDivElement = this.generateTextFieldTextareaWrapper(this.generatetextFieldTemplate(fabricTextObjectId));
        const textFieldTextarea: HTMLTextAreaElement = this.getTextareaFromTextFieldTextareaWrapper(textFieldTextareaWrapper);

        this.addToDOMtextField(textFieldContainer, textFieldTextareaWrapper);
        this.listenTextFieldRemove();
        this.listenGeneratedtextFieldTextarea(textFieldTextarea);

        return textFieldTextarea;
    }

    private listenTextFieldRemove() {
        $('[data-action="business-card.text-field-remove"]').click(
            (event) => this.onTextFieldRemoveRequested(event),
        );
    }

    private onTextFieldRemoveRequested(event: Event) {
        const target = $(event.currentTarget);
        const fabricObjectId = target.data('id');

        this.businessCardCanvases.canvas
            .getObjects('i-text')
                .forEach((object: fabric.Object) => {
                    if (object.data.id === fabricObjectId) {
                        this.businessCardCanvases.removeObject(object).then(() => {
                            target.closest('.bc-text-fields__list-item').remove();
                        }).catch(() => {});
                    }
                });
    }

    private listenChangeTextColor() {
       $('[data-action="business-card.text-color"]').colorpicker()
            .on('changeColor', (event) => {
                this.onColorpickerChangeColor(event, (selectedColor: string) => {
                    this.stylishActiveTextObjects((text: fabric.IText) => {
                        text.setFill(selectedColor);
                    });
                });
            });
    }

    private listenChangeTextBackgroundColor() {
        $('[data-action="business-card.text-background-color"]').colorpicker()
            .on('changeColor', (event) => {
                this.onColorpickerChangeColor(event, (selectedColor: string) => {
                    this.stylishActiveTextObjects((text: fabric.IText) => {
                        text.setTextBackgroundColor(selectedColor);
                    });
                });
            });
    }

    private listenTextEditingOnCanvas() {
        this.businessCardCanvases.forEach((canvas: fabric.Canvas) => {
            canvas.on(
                'text:editing:exited',
                (event: { e: Event, target: fabric.Text|fabric.IText }) => this.onTextOnCanvasEdited(event.target),
            );
        });
    }

    private listenTextRemoving() {
        this.businessCardCanvases.events.addListener(EVENT_CANVAS_OBJECT_REMOVED, (object: fabric.Object) => {
            const objectType = object.type;

            if (objectType === 'text' || objectType === 'i-text') {
                // If text was removed using keyboard we have superfluous form control in section 'Поля для ввода данных',
                // so we have to remove it
                const formControl = $(`.form-control[data-id="${object.data.id}"]`);

                if (formControl.length) {
                    formControl.closest('.bc-text-fields__list-item').remove();
                }
            }
        });
    }

    private listenRequestedLoadingFromString() {
        this.businessCardCanvases.events.addListener(
            EVENT_CANVAS_REQUESTED_LOADING_FROM_STRING, 
            () => this.emptyTextFieldsContainers(),
        );
    }

    private listenCanvasObjectLoadedFromString() {
        this.businessCardCanvases.events.addListener(
            EVENT_CANVAS_OBJECT_LOADED_FROM_STRING, 
            (object: {}, canvasSide: string) => {
                if (this.isObjectText((object as fabric.Object))) {
                    const fabricTextObjectId: string = object['data'].id;
                    const instertedTextarea = this.addTextToDOM(
                        this.textFieldsContainer[canvasSide],
                        fabricTextObjectId,
                    );

                    instertedTextarea.value = object['text'];
                }
            },
        );
    }

    private emptyTextFieldsContainers() {
        this.setTextFieldsContainersContent({
            front: '',
            back: '',
        });
    }

    private setTextFieldsContainersContent(content: { [propName: string]: string }) {
        Object.keys(this.textFieldsContainer).forEach((key: string) => {
            (this.textFieldsContainer[key] as HTMLElement).innerHTML = content[key];
        });
    }

    private onTextOnCanvasEdited(target: fabric.Text|fabric.IText) {
        $(`[data-id="${target.data.id}"]`).val(target.text);
    }

    private onColorpickerChangeColor(event, callback: (color: string) => any) {
        const selectedColor = event.color.toString('rgba');

        $(event.target).css('background-color', selectedColor);
        callback(selectedColor);
    }

    private generatetextFieldTemplate(id): string {
        return Mustache.render(this.templates.textField, {
            id,
        });
    }

    private generatetextFieldId(): string {
        return `text-${+new Date()}`;
    }

    private generateTextFieldTextareaWrapper(textFieldRenderedTemplate: string): HTMLDivElement {
        const textFieldContainer: HTMLDivElement = document.createElement('div');
        
        textFieldContainer.classList.add('bc-text-fields__list-item');
        textFieldContainer.innerHTML = textFieldRenderedTemplate;

        return textFieldContainer;
    }

    private getTextareaFromTextFieldTextareaWrapper(textFieldContainer: HTMLDivElement): HTMLTextAreaElement {
        return textFieldContainer.querySelector('textarea');
    }

    private addToDOMtextField(textFieldContainer: HTMLElement, textFieldTextarea: HTMLElement): void {
        textFieldContainer.appendChild(textFieldTextarea);
    }

    private listenGeneratedtextFieldTextarea(textFieldTextarea: HTMLTextAreaElement): void {
        textFieldTextarea.addEventListener('input', (event) => {
            const textarea: HTMLTextAreaElement = <HTMLTextAreaElement> event.target;
            const textareaId: string = textarea.dataset.id;
            const textareaValue: string = textarea.value;

            this.businessCardCanvases.canvas.getObjects('i-text').forEach((object: fabric.IText) => {
                if (object.data.id === textareaId) {
                    object.setText(textareaValue);
                    this.businessCardCanvases.renderAll();
                }
            });
        });

        textFieldTextarea.addEventListener('click', (event) => {
            const textarea: HTMLTextAreaElement = <HTMLTextAreaElement> event.target;
            const textareaId: string = textarea.dataset.id;

            this.businessCardCanvases.canvas.getObjects('i-text').forEach((object: fabric.IText) => {
                if (object.data.id === textareaId) {
                    this.businessCardCanvases.setActiveObject(object);
                }
            });
        });
    }

    private addTextToCanvas(value: string, id: string) {
        const selectedFontWeight = $('select[data-action="change-text-font-family"] option:checked').data('font-weight');
        const text = new fabric.IText(value, {
            lockScalingX: true,
            lockScalingY: true,
            lockUniScaling: true,
            lockScalingFlip: true,
            fontFamily: $('select[data-action="change-text-font-family"]').val(),
            fontSize: $('select[data-action="change-text-font-size"]').val(),
            fontWeight: selectedFontWeight,
            data: {
                fontWeightDefault: selectedFontWeight,
                id,
            },
        });

        this.businessCardCanvases.canvas.add(text);

        text.center();
        text.setCoords();
    }

    private stylishActiveTextObjects(action: (activeObject: fabric.Object) => any) {
        const activeObject = this.businessCardCanvases.getActiveObject();
        const activeGroup = this.businessCardCanvases.canvas.getActiveGroup();

        if (activeGroup) {
            return this.stylishActiveTextObjectsMany(activeGroup, action);
        } else if (activeObject) {
            return this.stylishActiveTextObjectsOne(activeObject, action)
        }
    }

    private stylishActiveTextObjectsMany(activeGroup: fabric.Group, action: (activeObject: fabric.Object) => any) {
        activeGroup.getObjects('i-text')
            .forEach((fabricITextObject: fabric.IText) => {
                action(fabricITextObject);
            });

        this.businessCardCanvases.renderAll();
    }

    private stylishActiveTextObjectsOne(activeObject: fabric.Object, action: (activeObject: fabric.Object) => any) {
        if (this.isObjectText(activeObject)) {
            action(activeObject);
            this.businessCardCanvases.renderAll();
        }
    }

    private isObjectText(object: fabric.Object) {
        return object && (object.type === 'text' || object.type === 'i-text');
    }

    private activeObjectIsNotTextError() {
        swal('Oops...', 'Select text object to stylish it', 'error');
    }

}
