import { EVENT_CANVAS_SIZE_CHANGED } from './../size/size.events';
import { EVENT_CANVAS_ROUNDED_VALUE_CHANGED } from './../rounded/rounded.events';
import { BusinessCardSize } from './../size/size';
import { BusinessCardCore } from '../core/core';
import { BusinessCardCanvases } from '../canvases/canvases';
import { IObjectOptions } from 'fabric';
import { BusinessCardRounded } from '../rounded/rounded';

export class BusinessCardOverlayImage extends BusinessCardCore {

    private selector: string = '[data-action="set-overlay"]';
    private element: NodeListOf<HTMLElement>|null = <NodeListOf<HTMLElement>|null> document.querySelectorAll(this.selector);
    private options: { overlayUrl: string } = {
        overlayUrl: `/bundles/app/images/overlays`,
    };

    constructor(public businessCardCanvases: BusinessCardCanvases,
                public businessCardSize: BusinessCardSize,
                public businessCardRounded: BusinessCardRounded) {
        super(businessCardCanvases);
        this.setInitialOverlayImage();
        this.listen();
        this.listenRoundedChange();
        this.listenSizeChange();
    }

    public setOverlayImage(url: string): void {
        const overlayImageOptions: IObjectOptions = {
            originX: 'left',
            originY: 'top',
            width: this.businessCardCanvases.canvas.getWidth(),
            height: this.businessCardCanvases.canvas.getHeight(),
        };

        this.businessCardCanvases.setLoadingState();

        this.businessCardCanvases.forEach((canvas) => {
            canvas.setOverlayImage(
                url,
                this.imageLoadedAndSetAsOverlayCallback.bind(this),
                overlayImageOptions,
            );
        });
    }

    private setInitialOverlayImage() {
        this.generateAndSetOverlayImageUrl();
    }

    private listenRoundedChange() {
        this.businessCardCanvases.events.addListener(EVENT_CANVAS_ROUNDED_VALUE_CHANGED, (isRounded: boolean) => {
            this.generateAndSetOverlayImageUrl();
        });
    }

    private listenSizeChange() {
        this.businessCardCanvases.events.addListener(EVENT_CANVAS_SIZE_CHANGED, (canvasSize: string) => {
            this.generateAndSetOverlayImageUrl();
        });
    }

    private generateAndSetOverlayImageUrl() {
        const size = `${this.businessCardSize.getActiveSizeInMM().width}x${this.businessCardSize.getActiveSizeInMM().height}`;

        if (this.businessCardRounded.getIsRounded()) {
            this.setOverlayImage(`${this.options.overlayUrl}/${size}-rounded.png`);
        } else {
            this.setOverlayImage(`${this.options.overlayUrl}/${size}.png`);
        }
    }

    private listen() {
        Array.prototype.forEach.call(this.element, (element: HTMLElement) => {
            element.addEventListener('click', (event: Event) => {
                this.setOverlayImage(element.getAttribute('data-url'));
            });
        });
    }

    private imageLoadedAndSetAsOverlayCallback(overlayImage: HTMLImageElement|null): void {
        if (this.isOverlaySuccessfullyLoaded(overlayImage)) {
            this.businessCardCanvases.renderAllOnAllCanvases();
        } else {
            console.error('Error during loading overlay image');
        }

        this.businessCardCanvases.unsetLoadingState();
    }

    private isOverlaySuccessfullyLoaded(overlayImage: HTMLImageElement|null): boolean {
        return overlayImage instanceof HTMLImageElement;
    }
}
