declare const wedes;

import * as f from 'fabric'; const fabric: typeof f = (f as any).fabric;
import swal from 'sweetalert2';
import { EventEmitter } from 'fbemitter';
import { EVENT_CANVAS_SIDE_CHANGED, EVENT_CANVAS_OBJECT_REMOVED, EVENT_CANVAS_REQUESTED_LOADING_FROM_STRING, EVENT_CANVAS_OBJECT_LOADED_FROM_STRING, EVENT_CANVAS_FINISHED_LOADING_FROM_STRING } from './canvases.events';
import fixCanvasImagesSrc from './fix-canvas-images-src';

export interface IJSONCanvas {
    sides: {
        front: string;
        back: string
    };

    dimensions: {
        width: number;
        height: number;
    };
}

export class BusinessCardCanvases {

    public canvas: fabric.Canvas;
    public canvasesContainer: HTMLElement = document.getElementById('canvases-business-cards-container');
    public canvases: {[propName: string]: fabric.Canvas} = {
        front: new fabric.Canvas('canvas-business-card-front'),
        back: new fabric.Canvas('canvas-business-card-back'),
    };
    public events: EventEmitter = new EventEmitter();

    private selectors: { sideToggle: string, saveToggle: string, importToggle: string } = {
        sideToggle: '[data-action="change-side"]',
        saveToggle: '[data-action="save-template"]',
        importToggle: '[data-action="import-template"]',
    };
    private elements: { sideToggle: NodeListOf<HTMLElement>|null, saveToggle: HTMLElement|null, importToggle: HTMLElement|null } = {
        sideToggle: <NodeListOf<HTMLElement>|null> document.querySelectorAll(this.selectors.sideToggle),
        saveToggle: <HTMLElement|null> document.querySelector(this.selectors.saveToggle),
        importToggle: <HTMLElement|null> document.querySelector(this.selectors.importToggle),
    };
    private activeCanvasSide: string = 'front';

    constructor() {
        this.changeCanvasSide(this.activeCanvasSide);
        this.listen();
        this.setBackgroundColor('#fff');
    }

    public forEach(action: (canvas: fabric.Canvas, key: string) => any): void {
        for (const key in this.canvases) {
            if (this.canvases.hasOwnProperty(key)) {
                action(this.canvases[key], key);
            }
        }
    }

    public changeCanvasSide(nextSide: string): void {
        this.updateCanvasesContainer(nextSide);
        this.markSideToggleAsActive(nextSide);
        this.setCanvasSide(nextSide);
        this.events.emit(EVENT_CANVAS_SIDE_CHANGED, nextSide);
    }

    public renderAll(): void {
        this.canvas.renderAll();
    }

    public renderAllOnAllCanvases(): void {
        this.forEach((canvas: fabric.Canvas) => {
            canvas.renderAll();
        });
    }

    public setLoadingState(): void {
        this.canvasesContainer.classList.add('loading');
    }

    public unsetLoadingState(): void {
        this.canvasesContainer.classList.remove('loading');
    }

    public getActiveObject(): fabric.Object {
        return this.canvas.getActiveObject();
    }

    public removeObject(object: fabric.Object): Promise<undefined> {
        return new Promise((resolve, reject) => {
            swal({
                title: wedes.translations.objectRemovingConfirmationWindow.title,
                text: wedes.translations.objectRemovingConfirmationWindow.message,
                confirmButtonText: wedes.translations.objectRemovingConfirmationWindow.confirmButtonText,
                confirmButtonColor: '#81081b',
                showCancelButton: true,
                cancelButtonText: wedes.translations.objectRemovingConfirmationWindow.cancelButtonText,
            }).then(() => {
                this.canvas.remove(object);
                this.events.emit(EVENT_CANVAS_OBJECT_REMOVED, object);
                resolve();
            }).catch(() => {
                reject();
            });
        });
    }

    public moveObject(parameters: { object: fabric.Object|undefined, direction: string, distance: number }) {
        if (parameters.object) {
            switch (parameters.direction) {
                case 'top':
                    parameters.object.top -= parameters.distance;
                break;
                case 'right':
                    parameters.object.left += parameters.distance;
                break;
                case 'bottom':
                    parameters.object.top += parameters.distance;
                break;
                case 'left':
                    parameters.object.left -= parameters.distance;
                break;
            }

            this.renderAll();
        }
    }

    public setActiveObject(object: fabric.Object) {
        this.canvas.setActiveObject(object);
    }

    public setBackgroundColor(color: string) {
        this.forEach((canvas: fabric.Canvas) => {
            this.canvas.backgroundColor = color;
        });
    }

    public getActiveCanvasSide(): string {
        return this.activeCanvasSide;
    }

    public toJSON(): {} {
        const canvasesObject: {} = {};

        canvasesObject['sides'] = {};
        this.forEach((canvas: fabric.Canvas, key: string) => {
            canvasesObject['sides'][key] = canvas.toDatalessJSON(['data']);
        });

        canvasesObject['dimensions'] = {
            width: this.canvas.getWidth(),
            height: this.canvas.getHeight(),
        };

        return fixCanvasImagesSrc(canvasesObject);
    }

    public toString() {
        return JSON.stringify(this.toJSON());
    }

    public fromString(stringCanvasData: string, callback?: Function): void {
        const JSONCanvasData = JSON.parse(stringCanvasData);
        this.events.emit(EVENT_CANVAS_REQUESTED_LOADING_FROM_STRING);

        this.fromJSON(JSONCanvasData, callback);
    }

    public fromJSON(JSONCanvasData, callback?: Function) {
        const dimensions = JSONCanvasData.dimensions;
        this.forEach((canvas: fabric.Canvas, key: string) => {
            canvas
                .setWidth(dimensions.width)
                .setHeight(dimensions.height)
                .loadFromJSON(JSONCanvasData.sides[key], () => {
                    this.renderAll();

                    if (callback) {
                        callback();
                    }

                    this.events.emit(EVENT_CANVAS_FINISHED_LOADING_FROM_STRING);
                }, (object: fabric.Object) => {
                    this.events.emit(EVENT_CANVAS_OBJECT_LOADED_FROM_STRING, object, key);
                });
        });
    }

    public toImages(): { [propName: string]: string|null } {
        const images = {};

        this.forEach((canvas: fabric.Canvas, key: string) => {
            if (canvas.getObjects().length) {
                images[key] = canvas.toDataURL({
                    format: 'png',
                });
            } else {
                images[key] = null;
            }
        });

        return images;
    }

    private listen() {
        Array.prototype.forEach.call(this.elements.sideToggle, (element: HTMLButtonElement) => {
            element.addEventListener('click', (event: Event) => {
                this.changeCanvasSide(element.getAttribute('data-side'));
            });
        });

        if (this.elements.saveToggle) {
            this.elements.saveToggle.addEventListener('click', (event) => {
                const JSONCanvas = JSON.stringify(this.canvas.toDatalessJSON());
                alert(JSONCanvas); console.log(JSONCanvas);
            });
        }

        if (this.elements.importToggle) {
            this.elements.importToggle.addEventListener('click', (event) => {
                const el: HTMLTextAreaElement = <HTMLTextAreaElement> document.getElementById('canvas-business-card-template-textarea');
                const canvasJSON = el.value;

                this.canvas.loadFromJSON(canvasJSON, this.canvas.renderAll);
            });
        }
    }

    private updateCanvasesContainer(nextSide: string): void {
        this.canvasesContainer.classList.remove(this.activeCanvasSide);
        this.canvasesContainer.classList.add(nextSide);
    }

    private setCanvasSide(nextSide: string): void {
        this.canvas = this.canvases[nextSide];
        this.activeCanvasSide = nextSide;
    }

    private markSideToggleAsActive(nextSide: string): void {
        Array.prototype.forEach.call(
            this.elements.sideToggle, 
            (element: HTMLElement) => {
                if (element.dataset.side !== nextSide) {
                    if (element.classList.contains('active')) {
                        element.classList.remove('active');
                    }
                } else {
                    element.classList.add('active');
                }
            },
        );
    }
}
