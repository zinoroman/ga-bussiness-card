/* Ad-hock solution
   Probably, there is a better way to do it */

/*
    We have to remove protocol and domain from images src to make app work on different domain
    Example
    Before:
    {..., src: protocol//sitename.domain/app_dev.php/path/to/image.png, ... }
    After
    { ..., src: /path/to/image.png, ...  }
*/
export default function fixCanvasImagesSrc(canvasesObject: {}): {} {
    const partToRemoveFromSrc = `${window.location.protocol}//${window.location.hostname}`;
    const fixSrc = (src: string) => src.replace(partToRemoveFromSrc, '');

    Object.keys(canvasesObject['sides']).forEach((key: string) => {
        // Fix src for images
        <Array<{}>> canvasesObject['sides'][key].objects.map((object: {}) => {
            if (object['type'] === 'image') {
                return Object.assign(object, {
                    src: fixSrc(object['src']),
                });
            } else {
                return object;
            }
        });

        // Fix src for overlay image
        if (canvasesObject['sides'][key]['overlayImage']) {
            canvasesObject['sides'][key]['overlayImage']['src'] = fixSrc(canvasesObject['sides'][key]['overlayImage']['src']);
        }
    });

    return canvasesObject;
}