import * as keyboardJS from 'keyboardjs';
import { BusinessCardCore } from '../core/core';
import { BusinessCardCanvases } from '../canvases/canvases';

interface BusinessCardKeyboardOptions {
    steps: { 
        [propertyName: string]: number,
    }, 
    keys: { 
        [propertyName: string]: string,
    },
}

export class BusinessCardKeyboard extends BusinessCardCore {

    private options: BusinessCardKeyboardOptions = {
        steps: {
            large: 10,
            normal: 5,
            small: 1,
        },
        keys: {
            normal: 'shift',
            small: 'alt',
        },
    }

    constructor(public businessCardCanvases: BusinessCardCanvases, userOptions?: BusinessCardKeyboardOptions) {
        super(businessCardCanvases);
        this.listen();

        if (userOptions) {
            this.options = Object.assign(this.options, userOptions);
        }
    }

    private listen() {
        this.listenRemove();
        this.listenMove();
    }

    private listenRemove() {
        keyboardJS.bind(['delete', 'backspace'], (event) => {
            const activeObject: fabric.Object|undefined = this.businessCardCanvases.getActiveObject();
            
            if (activeObject && ((event.target) as HTMLElement).isSameNode(document.body)) {
                this.businessCardCanvases.removeObject(activeObject);
            }
        });
    }

    private listenMove() {
        this.listenMoveTop();
        this.listenMoveRight();
        this.listenMoveBottom();
        this.listenMoveLeft();
    }

    private listenMoveTop() {
        keyboardJS.bind(
            ['up'], 
            (event) => this.moveActiveObject('top', this.options.steps.large)
        );
        keyboardJS.bind(
            [`${this.options.keys.normal} + up`], 
            (event) => this.moveActiveObject('top', this.options.steps.normal)
        );
        keyboardJS.bind(
            [`${this.options.keys.small} + up`], 
            (event) => this.moveActiveObject('top', this.options.steps.small)
        );
    }

    private listenMoveRight() {
        keyboardJS.bind(
            ['right'], 
            (event) => this.moveActiveObject('right', this.options.steps.large)
        );
        keyboardJS.bind(
            [`${this.options.keys.normal} + right`], 
            (event) => this.moveActiveObject('right', this.options.steps.normal)
        );
        keyboardJS.bind(
            [`${this.options.keys.small} + right`], 
            (event) => this.moveActiveObject('right', this.options.steps.small)
        );
    }

    private listenMoveBottom() {
        keyboardJS.bind(
            ['down'], 
            (event) => this.moveActiveObject('bottom', this.options.steps.large)
        );
        keyboardJS.bind(
            [`${this.options.keys.normal} + down`], 
            (event) => this.moveActiveObject('bottom', this.options.steps.normal)
        );
        keyboardJS.bind(
            [`${this.options.keys.small} + down`], 
            (event) => this.moveActiveObject('bottom', this.options.steps.small)
        );
    }

    private listenMoveLeft() {
        keyboardJS.bind(
            ['left'], 
            (event) => this.moveActiveObject('left', this.options.steps.large)
        );
        keyboardJS.bind(
            [`${this.options.keys.normal} + left`], 
            (event) => this.moveActiveObject('left', this.options.steps.normal)
        );
        keyboardJS.bind(
            [`${this.options.keys.small} + left`], 
            (event) => this.moveActiveObject('left', this.options.steps.small)
        );
    }

    private moveActiveObject(direction: string, distance: number): void {
        const object = this.businessCardCanvases.getActiveObject();

        if (object && ((event.target) as HTMLElement).isSameNode(document.body)) {
            this.businessCardCanvases.moveObject({
                object,
                direction,
                distance,
            });

            event.preventDefault();
        }
    }

}