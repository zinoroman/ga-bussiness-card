declare const wedes;

import { BusinessCardCore } from '../core/core';
import { BusinessCardCanvases } from '../canvases/canvases';
import swal from 'sweetalert2';
import * as Mustache from 'mustache';

export class BusinessCardOrder extends BusinessCardCore {

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);
        this.listen();
    }

    private listen() {
        this.listenOrderRequest();
        this.listenModal();
    }

    private listenOrderRequest() {
        $('[data-el="app_simple_order.modal"]').click(
            (event) => this.onOrderRequested(event),
        );
    }

    private listenModal() {
        $(document).on(
            'shown.bs.modal', 
            (event) => this.onModalShown(event),
        );
    }

    private onModalShown(event: Event) {
        const $targetModal = $(event.target);

        if (this.isSimpleOrderModal($targetModal)) {
            this.setBusinessCardImages($targetModal);
            this.setBusinessCardFormData($targetModal);

            // hack for modal alignement
            $(window).trigger('resize');
            // endhack
        }
    }

    private onOrderRequested(event: Event) {
        if (this.isPossibilityToCreateOrder()) {
            this.showOrderForm(event);
        } else {
            this.orderCannotBeCreatedWarning();
        }
    }

    private showOrderForm(event: Event) {
        const $element = $(event.currentTarget);
        
        $['modal$']({
            ajax: {
                url: $element.data('url'),
            },
        });
    }

    private isPossibilityToCreateOrder(): boolean {
        let isPossibility = false;

        this.businessCardCanvases.forEach((canvas: fabric.Canvas) => {
            if (canvas.getObjects().length) {
                isPossibility = true;
            }
        });

        return isPossibility;
    }

    private orderCannotBeCreatedWarning() {
        swal({
            type: 'error',
            title: wedes.translations.orderCannotBeCreatedError.title,
            text: wedes.translations.orderCannotBeCreatedError.message,
            confirmButtonColor: '#81081b',
        });
    }

    private isSimpleOrderModal(modal) {
        return modal.find('form[data-name="app_simple_order"]').length;
    }

    private setBusinessCardImages(modal: JQuery) {
        const images = this.businessCardCanvases.toImages();
        const imagesContainer = $('#simple-order-modal-business-card-preview');
        const previewTemplate = modal.find('#simple-order-modal-business-card-preview-template').html();

        Object.keys(images).forEach((key: string) => {
            if (images[key]) {
                imagesContainer.append(this.createPreview(previewTemplate, images[key]));
            }
        });
    }

    private createPreview(template: string, src: string) {
        return Mustache.render(template, {
            src,
        });
    }

    private setBusinessCardFormData(modal) {
        const businessCardDataInputSelector = '#request_frontend_businessCardData';

        modal
            .find(businessCardDataInputSelector)
            .val(this.businessCardCanvases.toString());
    }
}
