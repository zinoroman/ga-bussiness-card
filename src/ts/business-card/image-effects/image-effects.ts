import { BusinessCardControlPanelItem } from './../control-panel-item/control-panel-item';
import { BusinessCardCanvases } from '../canvases/canvases';
import * as f from 'fabric'; const fabric: typeof f = (f as any).fabric;

export class BusinessCardImageEffects extends BusinessCardControlPanelItem {

    public selector: string = '[data-action="business-card.image-effects"]';
    private element: JQuery = $(this.selector);

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);
        this.listen();
    }

    private listen() {
        if (this.canEventListenerBeAttached(this.element[0])) {
            this.element.change((event) => this.onEffectChanged(event));

            this.businessCardCanvases.forEach((canvas: fabric.Canvas) => {
                canvas.on('object:selected', (event: { e: Event, target: fabric.Object }) => this.onObjectSelected(event.target));
                canvas.on('selection:cleared', (event: { e: Event, target: fabric.Object }) => this.onObjectUnselected());
            });
        }
    }

    private onObjectSelected(object: fabric.Object) {
        if (this.isObjectImage(object) && this.isFilterApplied(object as fabric.Image)) {
            const activeFilterName: string = this.getActiveFilterName(object as fabric.Image);
            this.element.selectpicker('val', activeFilterName);
        } else {
            this.resetEffectsToggleValue();
        }
    }

    private onObjectUnselected() {
        this.resetEffectsToggleValue();
    }


    private isFilterApplied(image: fabric.Image): boolean {
        return !!image.filters.length;
    }

    private getActiveFilterName(image: fabric.Image): string {
        return image.filters[0]['type'];
    }

    private resetEffectsToggleValue() {
        this.element.selectpicker('val', '');
    }

    private onEffectChanged(event: Event) {
        const activeObject: fabric.Object = this.businessCardCanvases.getActiveObject();
        const filterType: string = $(event.target).val();

        if (this.isObjectImage(activeObject) && filterType) {
            activeObject['filters'] = [];
            activeObject['filters'].push(new fabric.Image.filters[filterType](this.generateFilterOptions(filterType)));

            activeObject['applyFilters'](() => {
                this.businessCardCanvases.renderAll();
            });
        }
    }

    private generateFilterOptions(filterType: string): any {
        switch(filterType) {
            case 'Brightness':
                return { brightness: 50 };
            case 'Convolute':
                return { matrix: [1, 1, 1, 1, .7, -1, -1, 1, -1] };
            case 'Noise':
                return { noise: 100 };
            case 'Tint':
                return { color: '#3513B0', opacity: .5 }
            default:
                return null;
        }
    }

    private isObjectImage(object: fabric.Object) {
        return object && object.type === 'image';
    }
}