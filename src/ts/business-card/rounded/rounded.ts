import { EVENT_CANVAS_ROUNDED_VALUE_CHANGED } from './rounded.events';
import { BusinessCardCore } from '../core/core';
import { BusinessCardCanvases } from '../canvases/canvases';

export class BusinessCardRounded extends BusinessCardCore {

    private isRounded: boolean = false;
    private options = {
        toggleSelector: '[data-action="business-card.rounded"]',
    };

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);
        this.listen();
    }

    public getIsRounded(): boolean {
        return this.isRounded;
    }

    private listen() {
        const toggle = <HTMLElement|null> document.querySelector(this.options.toggleSelector);

        if (this.canEventListenerBeAttached(toggle)) {
            toggle.addEventListener('change', (event) => this.onRoundedValueChanged(event));
        }
    }

    private canEventListenerBeAttached(toggle: HTMLElement|null) {
        return !!toggle;
    }

    private onRoundedValueChanged(event: Event) {
        const toggle = <HTMLInputElement> event.target;

        if ((event.target as HTMLInputElement).checked) {
            this.isRounded = true;
            this.businessCardCanvases.canvasesContainer.classList.add('rounded');
        } else {
            this.isRounded = false;
            this.businessCardCanvases.canvasesContainer.classList.remove('rounded');
        }

        this.businessCardCanvases.events.emit(EVENT_CANVAS_ROUNDED_VALUE_CHANGED, this.isRounded);
    }
}
