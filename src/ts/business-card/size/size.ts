import { EVENT_CANVAS_SIZE_CHANGED } from './size.events';
import { BusinessCardCore } from '../core/core';
import { BusinessCardCanvases } from '../canvases/canvases';

export class BusinessCardSize extends BusinessCardCore {

    private activeSizeInMM: { width: number, height: number } = {
        width: 90,
        height: 50,
    };
    private options = {
        toggleSelector: '[data-action="business-card.change-size"] .btn',
        sizeCoefficient: 600 / 90,
        separator: 'x',
    };

    constructor(public businessCardCanvases: BusinessCardCanvases) {
        super(businessCardCanvases);
        this.listen();
    }

    public setActiveSizeInMM(activeSize: { width: number, height: number }) {
        this.activeSizeInMM = activeSize;
    }

    public getActiveSizeInMM(): { width: number, height: number } {
        return this.activeSizeInMM;
    }

    private listen() {
        const toggles = <NodeListOf<HTMLElement>> document.querySelectorAll(this.options.toggleSelector);

        if (this.canEventListenerBeAttached(toggles)) {
            Array.prototype.forEach.call(toggles, (toggle: HTMLElement) => {
                toggle.addEventListener('click', (event) => this.onSizeChanged(event));
            });
        } else {
            this.toggleIsAbsentError();
        }
    }

    private canEventListenerBeAttached(toggle: NodeListOf<HTMLElement>): boolean {
        return !!toggle.length;
    }

    private onSizeChanged(event: Event) {
        const toggle = <HTMLInputElement> (event.currentTarget as HTMLElement).querySelector('input[type="radio"]');
        const toggleValue: string = toggle.value;
        const dimensionsInMM: string[] = toggleValue.split(this.options.separator);
        const newDimensions: { width: number, height: number } = {
            width: Math.round(parseInt(dimensionsInMM[0], 10) * this.options.sizeCoefficient),
            height: Math.round(parseInt(dimensionsInMM[1], 10) * this.options.sizeCoefficient),
        };

        this.businessCardCanvases.forEach((canvas: fabric.Canvas) => {
            canvas.setDimensions({
                width: newDimensions.width,
                height: newDimensions.height,
            }).renderAll();
        });

        this.setActiveSizeInMM({
            width: parseInt(dimensionsInMM[0], 10),
            height: parseInt(dimensionsInMM[1], 10),
        });

        this.notifyAboutSizeChange();
    }

    private notifyAboutSizeChange() {
        this.businessCardCanvases.events.emit(EVENT_CANVAS_SIZE_CHANGED, this.activeSizeInMM);
    }

    private toggleIsAbsentError() {
        throw new Error(`${this.options.toggleSelector} element is required for proper work`);
    }
}
